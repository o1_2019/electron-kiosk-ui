import bluetooth
import sys
import json
import random

# nearby_devices = bluetooth.discover_devices(duration=8, lookup_names=True, flush_cache=True, lookup_class=False)

# print(f"found {len(nearby_devices)} devices")

# for d in nearby_devices:
#     print(d)

uuid = str(sys.argv[1])

service_matches = bluetooth.find_service(uuid=uuid)
if len(service_matches) == 0:
    print("cannot find device")
    sys.exit(0)

first_match = service_matches[0]
port = first_match["port"]
name = first_match["name"]
host = first_match["host"]

socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
socket.connect((host, port))

data = {'type': 'uuid', 'data': uuid}
# Step 1: Send back UUID
socket.send(json.dumps(data))
data = socket.recv(2048)
data += socket.recv(2048)

data = json.loads(data.decode('utf-8'))
print(data['data'])

kiosk_data = {'type': 'success', 'data': str(random.randint(0, 100))}
socket.send(json.dumps(kiosk_data))

socket.close()
sys.stdout.flush()