import { Component } from '@angular/core';
import { SocketsService } from './_services/sockets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'electron-kiosk-app';

  constructor() {
  }
}
