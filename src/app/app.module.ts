import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RaceInfoComponent } from './race-info/race-info.component';
import { RaceListComponent } from './race-list/race-list.component';
import { BetSlipComponent } from './bet-slip/bet-slip.component';
import { BetSlipCardComponent } from './bet-slip-card/bet-slip-card.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatSidenavModule, MatToolbarModule, 
  MatIconModule, MatListModule, MatGridListModule, MatCardModule,
  MatInputModule, MatDialogModule,
  MatBadgeModule} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';

import {MatExpansionModule} from '@angular/material/expansion';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CashOutComponent } from './cash-out/cash-out.component';
import { MyBetsComponent } from './my-bets/my-bets.component';
import { TopRacesComponent } from './top-races/top-races.component';
import { LoginComponent } from './login/login.component';
import { ScannerComponent } from './scanner/scanner.component';
import { HeaderComponent } from './header/header.component';
import { NgQrScannerModule } from 'angular2-qrscanner';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { WebBluetoothModule } from '@manekinekko/angular-web-bluetooth';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { HorseDetailService } from './_services/race-service/horse-detail.service';
import { RaceInfoService } from './_services/race-service/race-info.service';
import { HorseInfoComponent } from './horse-info/horse-info.component';
import { HorseDetailComponent } from './horse-info/horse-detail/horse-detail.component';
import { PaymentStatusComponent } from './payment-fail/payment-fail.component';
import { PrintComponent } from './place-order/print/print.component';
import { FormsModule } from '@angular/forms';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';

import { HttpConfigInterceptor} from './_interceptor/httpconfig.interceptor';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { ErrorDialogService } from './error-dialog/error-dialog.service';


// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

// const config: SocketIoConfig = { url: 'http://localhost:4444', options: { } };


@NgModule({
  declarations: [
    AppComponent,
    RaceInfoComponent,
    RaceListComponent,
    BetSlipComponent,
    BetSlipCardComponent,
    NavigationComponent,
    PageNotFoundComponent,
    CashOutComponent,
    MyBetsComponent,
    TopRacesComponent,
    LoginComponent,
    ScannerComponent,
    HeaderComponent,
    PlaceOrderComponent,
    HorseInfoComponent,
    HorseDetailComponent,
    PaymentStatusComponent,
    PrintComponent,
    LoadingSpinnerComponent,
    ErrorDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    NgQrScannerModule,
    ZXingScannerModule,
    MatExpansionModule,
    MatChipsModule,
    FormsModule,
    WebBluetoothModule.forRoot({
      enableTracing: true // or false, this will enable logs in the browser's console
    }),
    MatInputModule,
    HttpClientModule,
    MatBadgeModule,
    MatDialogModule
    // SocketIoModule.forRoot(config)
  ],
  providers: [
    RaceInfoService, 
    HorseDetailService,
    ErrorDialogService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ErrorDialogComponent]
})
export class AppModule { }
