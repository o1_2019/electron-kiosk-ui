import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { BettingService } from '../_services/betting.service';
import { RaceInfoService } from './../_services/race-service/race-info.service';
import { raceInfo } from '../models/race-info.model';
import { HorseDetail } from '../models/horse-detail.model';
import { HorseDetailService } from '../_services/race-service/horse-detail.service';
import { HttpService } from '../_services/http.service';
import { GET_HORSE_DETAILS } from '../_constants/app.constants';
import { SocketsService } from '../_services/sockets.service';

@Component({
  selector: 'app-race-info',
  templateUrl: './race-info.component.html',
  styleUrls: ['./race-info.component.scss']
})
export class RaceInfoComponent implements OnInit, AfterViewInit {

  @Input() race: raceInfo;

  temp: any [] = [];

  horseDetails: HorseDetail[] = [];
  startDate: Date;
  endDate: Date;
  startTime: any;
  endTime: any;
  panelOpenState = false;

  constructor( private raceService: RaceInfoService,
              private httpService: HttpService,
              private socketService: SocketsService ) { }

  ngOnInit() {

    // this.socketService.getData().then(socketData => {
    //   console.log("inside component", socketData['data']);
    // }, error => {
    //   console.log(error);
    // });

    this.socketService.getHorseData({
      "horses": (this.race["contestants"])
    }).subscribe((data) => {
      this.horseDetails = JSON.parse(data["data"])["horse_details"];
      
      // console.log("data inside component - ", this.horseDetails);
      this.temp = [];
      this.horseDetails.forEach((el) => {
           this.temp.push(el);
      });
    }, (error) => {
      console.error(error);
    })

    // this.socketService.getHorseData({
    //   "horses": (this.race["contestants"])
    // }).then((data) => {
      // console.log("horse details - ", JSON.parse(data["data"]), JSON.parse(data["data"])["horse_details"]);
      // this.horseDetails = JSON.parse(data["data"])["horse_details"];
      // this.horseDetails.forEach((el) => {
      //      this.temp.push(el);
      // });
    // }, (error) => {
    //   console.error(error);
    // })
    // this.httpService.postRequest(GET_HORSE_DETAILS, {
    //   "horse_ids": this.race["contestants"]
    // }).subscribe((data) => {

    //   // data['response'].forEach(element => {
    //   //   this.horseDetails.push(element);
    //   // });
    //   this.horseDetails = data['horse_details'];
    //   this.horseDetails.forEach((el) => {
    //        this.temp.push(el);
    //   });
    //   //console.log( this.horseDetails);

    // }, error => {
    //   console.log("error ", error);
    // });
    this.startDate = new Date(this.race.startTime);
    this.startTime = this.startDate.toLocaleTimeString();
    this.endDate = new Date(this.race.endTime);
    this.endTime = this.endDate.toLocaleTimeString();
    //console.log(this.horseDetails);
    //console.log(this.race);
   }
   ngAfterViewInit() {

   }
}
