export class raceInfo{

        _id: {timestamp: number, 
            machineIdentifier: number, 
            processIdentifier: number, 
            counter: number, 
            time: number, 
            date: string, 
            timeSecond: number};
        raceId : number;
        raceName : string;
        startTime : string;
        endTime : string;
        contestants: any[];
}