
export class HorseDetail {

    public horse_details: HorseInfo[];
}

export class HorseInfo {
    public "_id": {timestamp: number, machineIdentifier: number,processIdentifier: number,counter: number,
        date: string, time: number, timeSecond: number };
        public horse_id: number;
        public horse_name: string;
        public last_5_races: any[];
        public win_multiplier: number;
        public lose_multiplier: number;
}
