import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { HorseDetail } from '../models/horse-detail.model';
import { BettingService } from '../_services/betting.service';
import { HorseDetailComponent } from './horse-detail/horse-detail.component';

@Component({
  selector: 'app-horse-info',
  templateUrl: './horse-info.component.html',
  styleUrls: ['./horse-info.component.scss']
})
export class HorseInfoComponent implements OnInit {

    @Input() horses: any[] = [];
    @Input() raceDetail: any;
    @ViewChild(HorseDetailComponent) ref: HorseDetailComponent;

    constructor(private bettingService: BettingService) { }

    ngOnInit() {
      // this.bettingService.removeBetSlip.subscribe((data) => {
      //   //alert(data);
      //   this.ref.addedTocart = false;
      //   console.log(data);
      // });
      // console.log(this.raceDetail);

    }

    addBet() {
      this.bettingService.addBet(this.horses);
    }
}
