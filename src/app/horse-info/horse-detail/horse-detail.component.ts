import { Component, OnInit, Input } from '@angular/core';
import { HorseInfo } from 'src/app/models/horse-detail.model';
import { BettingService } from 'src/app/_services/betting.service';

@Component({
  selector: 'app-horse-detail',
  templateUrl: './horse-detail.component.html',
  styleUrls: ['./horse-detail.component.scss']
})
export class HorseDetailComponent implements OnInit {
  @Input() horse: HorseInfo;
  @Input() raceDetail: any;

  likeFlag = true;
  addedTocart = false;
  propertyNames: { btnName: string, iconName: string} = {
    btnName: 'Bet Now !',
    iconName: 'favorite_border'
  };
  i = 1;

  constructor(private bettingService: BettingService) {
      
   }

  ngOnInit() {
  }

  addBet() {
    let tempHorse = Object.assign({}, this.horse);
    
    tempHorse["race_name"] = this.raceDetail.raceName;
    tempHorse["race_id"] = this.raceDetail.raceId;
    tempHorse["place_predicted"] = 1;
    this.bettingService.addBet(tempHorse);
    console.log(this.raceDetail);
    console.log(this.bettingService.getBetsAdded());
    this.i++;
    this.addedTocart = true;
    this.propertyNames.btnName = 'Bet Placed !';
    this.bettingService.removeBetSlip.subscribe((data) => {
      //alert(data);
      if (data === this.horse.horse_id) {
        this.addedTocart = false;
        this.propertyNames.btnName = 'Bet Now !';
      }
      console.log(data);
      console.log(this.horse);
    });

  }

  likeIt() {

    if (this.likeFlag) {
      this.likeFlag = false;
      this.propertyNames.iconName = 'favorite';
    } else {
      this.likeFlag = true;
      this.propertyNames.iconName = 'favorite_border';
    }
  }

}
