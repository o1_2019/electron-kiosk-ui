import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import * as jwt_decode from "jwt-decode";
import { getToken } from '@angular/router/src/utils/preactivation';
import { TOKEN_NAME } from 'src/app/_constants/app.constants';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private router: Router,
    private route: ActivatedRoute) { }

    loginAllowed = new Subject();
    loginEmail = new Subject();
    logoutFinal = new Subject();

  redirectToLogin() {
    if(this.router.url !== "/login" && this.router.url !== "/scanner") {
      this.router.navigate(['/login'], { relativeTo: this.route}); 
    }
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
    
  }
  public isAuthenticated(): boolean {
    // get the token
    // const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
    const token = this.getToken();
    if(token == undefined || token == null) {
      return false;
    } else {
      const decodedToken = this.decodeAccessToken(token);

      // return a boolean reflecting 
      // whether or not the token is expired
      if(decodedToken == undefined || decodedToken == null) {
        return false;
      }
      else {
        let currTime = new Date();
      if ( Date.now() / 1000 > decodedToken['exp']) {      
        return false;
      } else {
        return true;
      }
      }
    }
    
  }

  decodeAccessToken(token: string): any {
    try{
        return jwt_decode(token);   
    }
    catch(Error){
        return null;
    }
  }
}
