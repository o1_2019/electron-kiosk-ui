import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';

@Injectable({
  providedIn: 'root'
})
export class BluetoothService {

  private ipc: IpcRenderer;

  constructor() {
    if ((<any>window).require) {
      try {
        this.ipc = (<any>window).require("electron").ipcRenderer;
      } catch (error) {
        throw error;
      }
    } else {
      console.warn("Could not load electron ipc");
    }
  }

  async sendMsg(uuid: string) {
    return new Promise<string[]>((resolve, reject) => {
      console.log("received bluetooth request");
      this.ipc.send("sendBluetoothMsg", uuid);
      this.ipc.once("bluetoothToken", (event, arg) => {
        console.log(event, arg);
        resolve(arg);
      });
    });
  }
}
