import { raceInfo } from './../../models/race-info.model';
export class RaceInfoService {

    raceInfo :raceInfo[] = [{

        "_id": {
    
            "timestamp": 1551695471,
    
            "machineIdentifier": 7304930,
    
            "processIdentifier": 24109,
    
            "counter": 10206560,
    
            "time": 1551695471000,
    
            "date": "2019-03-04T10:31:11.000+0000",
    
            "timeSecond": 1551695471
    
        },
    
       "raceId": 1,
    
        "raceName": "Battle of Warriors",
    
        "startTime": "2019-03-10T04:30:00.000+0000",
    
        "endTime": "2019-03-10T05:30:00.000+0000",
    
        "contestants": [
    
            1,
    
            2,
    
            3
    
      ]},
      {

        "_id": {
    
            "timestamp": 1551695471,
    
            "machineIdentifier": 7304930,
    
            "processIdentifier": 24109,
    
            "counter": 10206560,
    
            "time": 1551695471000,
    
            "date": "2019-03-04T10:31:11.000+0000",
    
            "timeSecond": 1551695471
    
        },
    
       "raceId": 1,
    
        "raceName": "Battle of Bastards",
    
        "startTime": "2019-03-10T04:30:00.000+0000",
    
        "endTime": "2019-03-10T05:30:00.000+0000",
    
        "contestants": [
    
            1,
    
            2,
    
            3
    
      ]}];

    fetchAllRaces(){
        return this.raceInfo.slice();
    }

}
