import { HorseDetail, HorseInfo } from './../../models/horse-detail.model';

export class HorseDetailService {

    horseDetails: HorseDetail[] =[
        {
            "horse_details": [
        
                {
        
                    "_id": {
        
                        "timestamp": 1551695733,
        
                        "machineIdentifier": 7304930,
        
                        "processIdentifier": 24109,
        
                        "counter": 10206562,
        
                        "date": "2019-03-04T10:35:33.000+0000",
        
                        "time": 1551695733000,
        
                        "timeSecond": 1551695733
        
                    },
        
                    "horse_id": 1,
        
                    "horse_name": "John Snow",
        
                    "last_5_races": [
        
                        1,
        
                        1,
        
                        1,
        
                        1,
        
                        1
        
                    ],
        
                    "win_multiplier": 1,
        
                    "lose_multiplier": 1
                },{
        
                    "_id": {
        
                        "timestamp": 1551695733,
        
                        "machineIdentifier": 7304930,
        
                        "processIdentifier": 24109,
        
                        "counter": 10206562,
        
                        "date": "2019-03-04T10:35:33.000+0000",
        
                        "time": 1551695733000,
        
                        "timeSecond": 1551695733
        
                    },
        
                    "horse_id": 2,
        
                    "horse_name": "John Rain",
        
                    "last_5_races": [
        
                        1,
        
                        1,
        
                        1,
        
                        1,
        
                        1
        
                    ],
        
                    "win_multiplier": 1,
        
                    "lose_multiplier": 1
                }
            ]
        }
    ];

    fetchAllHorses() {
        return this.horseDetails.slice();
    }
}