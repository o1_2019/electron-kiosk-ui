import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/';

@Injectable({
  providedIn: 'root'
})
export class BettingService {
  private betsAdded: Array<{}> = new Array();

  removeBetSlip = new Subject();
  removeSingleBet = new Subject();
  perBetAmount = new Subject();

  constructor() { }

  addBet(betObj) {
    console.log(betObj);
    this.betsAdded.push({'betId': betObj.horse_id, 'value': betObj});
  }

  getBetsAdded() {
    return this.betsAdded;
  }

  removeBet(betObj) {
    let betId = betObj.betId;
    // console.log(this.betsAdded);
    for (var i = 0; i < this.betsAdded.length; i++) {
      if (this.betsAdded[i]['betId'] && this.betsAdded[i]['betId'] === betId) { 
        this.betsAdded.splice(i, 1);
        this.removeBetSlip.next(betId);
        return true;
      }
    }
    return false;
  }
}
