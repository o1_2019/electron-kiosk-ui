import { Component, OnInit, Input } from '@angular/core';
import { BettingService } from '../_services/betting.service';

@Component({
  selector: 'app-my-bets',
  templateUrl: './my-bets.component.html',
  styleUrls: ['./my-bets.component.scss']
})
export class MyBetsComponent implements OnInit {
  @Input() bet: any;
  amount: number = 0;
  prevAmount: number = 0;

  constructor(private bettingService: BettingService) { }

  ngOnInit() {
  }

  onKeyPress(event: any) {
  this.amount = +event.target.value;
  this.bet.value.betAmount = this.amount;
  //console.log(this.amount);
  const diff = this.amount - this.prevAmount;
  this.bettingService.perBetAmount.next(diff);
  this.prevAmount = this.amount;
  }

  onAdd(e: HTMLInputElement) {
    this.amount += 100;
    e.value = (this.amount).toString();
    //console.log(this.amount);
    this.bet.value.betAmount = this.amount;
    const diff = this.amount - this.prevAmount;
    this.bettingService.perBetAmount.next(diff);
    this.prevAmount = this.amount;
  }
  onSub(e: HTMLInputElement) {
    this.amount -= 100;
    e.value = (this.amount).toString();
    this.bet.value.betAmount = this.amount;
    //console.log(this.amount);
    const diff = this.amount - this.prevAmount;
    this.bettingService.perBetAmount.next(diff);
    this.prevAmount = this.amount;
  }

  removeBet() {
    this.bettingService.removeSingleBet.next(this.bet);
  }

}
