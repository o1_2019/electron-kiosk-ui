import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../_services/http.service';
import { AuthenticationService } from '../_services/authentication.service';
import { BluetoothService } from '../_services/bluetooth.service';
import { TOKEN_NAME } from '../_constants/app.constants';
//import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {

  @ViewChild('scanner') scanner: any ;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private httpService: HttpService, 
              private authService: AuthenticationService,
              private bluetoothService: BluetoothService) { }

  ngOnInit() {
  }

  camerasFoundHandler(event) {
    this.scanner.scan(event[0].deviceId);
  }

  scanCompleteHandler(event) {
    console.log("qrcode text - ", event.text);
    alert(" Please wait while we authenicate you...........");
    // const obj = JSON.parse(event.text)['data'].emailId;
    // this.authService.loginEmail.next(obj);
    //console.log(obj['data'].emailId);

    // this.bluetoothService.sendMsg("uuid").then((result) => {
    //   console.log("res - ", result);
    // });

    this.httpService.login({
        'qrcode': event.text
      }).subscribe(
      result1 => {
        console.log(" response 12 - ", result1);
        let data = event.text;
        console.log("Login successful ", data);
        if (data) {
          // const obj = JSON.parse(event.text)['data'].emailId;
          const uuid = JSON.parse(event.text)["data"].uuid;
          console.log("uuid - ", uuid)
          // this.authService.loginEmail.next(obj);
          //console.log(obj['data'].emailId);
          // this.bluetoothService.sendMsg().then(console.log);
          this.bluetoothService.sendMsg(uuid).then((result) => {
            let token = result.toString();
            console.log("token ", result);

            localStorage.setItem(TOKEN_NAME, token);
            localStorage.setItem("decodedToken", JSON.stringify(this.authService.decodeAccessToken(token)));
            this.router.navigate(['/home'], { relativeTo: this.route});

            // localStorage.setItem("Username", obj);
          });

        }
      },
      error => {
        console.log("Login error", error);
        //this.router.navigate(['/login', true] , { relativeTo: this.route});
        // this.authService.loginAllowed.next();
      }
    );
    // this.router.navigate(['/home'], { relativeTo: this.route});
  }
}
