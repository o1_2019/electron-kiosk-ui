import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopRacesComponent } from './top-races.component';

describe('TopRacesComponent', () => {
  let component: TopRacesComponent;
  let fixture: ComponentFixture<TopRacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopRacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopRacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
