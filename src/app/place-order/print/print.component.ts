import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-print',
  templateUrl: './print.component.html',
  styleUrls: ['./print.component.scss']
})
export class PrintComponent implements OnInit {

  id = '';
  player = '';
  amount = '';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.route.snapshot.params['id']);
    this.id = this.route.snapshot.params['id'];
    this.player = this.route.snapshot.params['pay'];
    console.log(this.route.snapshot.params['pay']);
    this.amount = this.route.snapshot.params['amnt'];

  }

}
