import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../_services/http.service';
import { SCAN_VOUCHER_URL, GET_PAYMENT_DETAILS, ORDER_UPDATE_URL } from '../_constants/app.constants';
import { AuthenticationService } from '../_services/authentication.service';
import { NavigationComponent } from '../navigation/navigation.component';
import { BettingService } from '../_services/betting.service';
declare let jsPDF;

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {

  @ViewChild('scanner') scanner: any;
  @ViewChild('title') title: any;
  cashVoucher: boolean;
  paymentStatus: boolean = false;
  paymentId: string;
  orderId: string;
  payerId: string;
  remainingBetAmount: number;
  actualAmount: number;
  paymentMessage: string = "";
  flag: boolean = false;
  confirmButton: boolean = false;
  qrCode: string;
  showSpinner: boolean = false;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private httpService: HttpService, 
    private authService: AuthenticationService,
    private bettingService: BettingService) { 
      this.cashVoucher = false;
      // this.paymentStatus = 'not paid';
    }

  ngOnInit() {
    this.route.queryParams.subscribe( params => {
      // console.log(+params["amount"]);
      this.remainingBetAmount = params["amount"];
      this.actualAmount = params["amount"];
    });
    // console.log(this.paymentStatus);
    // console.log(this.router.url);
    // console.log(" bets placed - ", this.bettingService.getBetsAdded());

    this.route.queryParams.subscribe(params => {
      // console.log(params);
      if ("paymentId" in params && "token" in params && "PayerID" in params) {
        this.paymentStatus = true;
        this.paymentId = params["paymentId"];
        this.payerId = params["PayerID"];
        // console.log("after payment - ", localStorage.getItem("betsPlaced"))
        
        this.paymentMessage = "Congratulations! Your order is successful. Your payment ID is <b>"+this.paymentId+"</b>";
      } else {
        
        localStorage.setItem("betsPlaced", JSON.stringify(this.bettingService.getBetsAdded()));
        // sessionStorage.setItem("betsPlaced", JSON.stringify(this.bettingService.getBetsAdded()));
        let betsInfo = JSON.parse(localStorage.getItem("betsPlaced"));
        let orderUpdateReqBody = {
          // "id": localStorage.getItem("decodedToken")["userId"],
          "betsPlaced": betsInfo,
          // "id": JSON.parse(localStorage.getItem("decodedToken"))["id"]
          "id": "dbsheta@gmail.com"
        };
        console.log("order update req body - ", orderUpdateReqBody);
        this.httpService.postRequest(ORDER_UPDATE_URL, orderUpdateReqBody).subscribe(
          data => {
            setTimeout(() => {
              this.showSpinner = false;
            }, 2000);
            if (data["transaction"] == "success") {
              console.info("Transaction successful");              
            } else {
              console.error("Error while updating the order in the backend");
              console.error(data["error"]);
            }
          },
          error => { 
            setTimeout(() => {
              this.showSpinner = false;
            }, 2000);
            console.error("Error while getting payment details", error);
          });
        console.log("bets placed added to local", localStorage.getItem("betsPlaced"));
      }
  });
  }

  camerasFoundHandler(event) {
    this.scanner.scan(event[0].deviceId);     
  }

  scanCompleteHandler(event) {
    if(this.flag == false){
      // console.log();
      // console.log(this.title);
      this.scanner.previewElemRef.nativeElement.style.display = 'none';
      this.title.nativeElement.style.display = 'none';
      // document.getElementById('scanner').style.display = 'none';

      event.text = JSON.parse(`{  
        "voucherId":"voucher2",
        "userName":1,
        "amount":100
     }`);

      this.qrCode = event.text;

      let temp = JSON.parse(JSON.stringify(event.text));
      // let userName = temp.userName;
      // let amount = temp.amount;
      // console.log(temp, userName)
      this.flag = true;
      this.confirmButton = true;
      this.paymentMessage = "Your cash voucher has been used successfully. The cash balance in your voucher is <b>Rs" + event.text.amount + "</b><br> Do you want to use?";
      console.log("Calculate remaining amount", this.remainingBetAmount);        
    }
        
  }

  useCashVoucher() {
    this.cashVoucher = true;
  }

  useCashVoucherUtil() {
    this.showSpinner = true;
    this.confirmButton = false;
    this.showSpinner = false;
      this.remainingBetAmount = +this.remainingBetAmount - this.qrCode["amount"];
         console.log("my amount " + this.remainingBetAmount);
        if(this.remainingBetAmount > 0) {
          this.cashVoucher = true;
          this.paymentMessage = "Your voucher has been used successfully. Redirecting you for the payment of remaining amount";
          this.payOnline();
          // this.router.navigate(['/order'], { 
          //   queryParams: {
          //   a: 1
          //   }
          // });
          //navigate to the paypal URL
        } else if (this.remainingBetAmount <= 0) {
          // make request checkout microservice and get order id
          this.paymentMessage = "Congratulations! Your order is successful. Your payment ID is <b>" + "PAYID-LSOIOXA13036246BM383854J" +"</b>";
          this.paymentStatus = true;
        } 
    // this.httpService.getVoucherRequest(SCAN_VOUCHER_URL, { 
    //   params: {
    //     'userName': this.qrCode["userName"],
    //     'voucherID': this.qrCode["voucherId"]
    //   }
    // }).subscribe(
    // data => {
    //   // console.log(data, data["amount"]); 
    //   // this.useCashVoucherUtil();
    //   this.showSpinner = false;
    //   this.remainingBetAmount = +this.remainingBetAmount - this.qrCode["amount"];
    //      console.log("my amount " + this.remainingBetAmount);
    //     if(this.remainingBetAmount > 0) {
    //       this.cashVoucher = true;
    //       this.paymentMessage = "Your voucher has been used successfully. Redirecting you for the payment of remaining amount";
    //       this.payOnline();
    //       // this.router.navigate(['/order'], { 
    //       //   queryParams: {
    //       //   a: 1
    //       //   }
    //       // });
    //       //navigate to the paypal URL
    //     } else if (this.remainingBetAmount <= 0) {
    //       // make request checkout microservice and get order id
    //       this.paymentMessage = "Congratulations! Your order is successful. Your payment ID is <b>" + data["paymentId"] +"</b>";
    //       this.paymentStatus = true;
    //     } 
    // },
    // error => {
    //   console.log("Error while using cash voucher", error);
    //   setTimeout(() => {
    //     this.showSpinner = false;
    //   }, 2000);
    //   this.router.navigate(['/order'], { relativeTo: this.route}); 
    // }
    // );
    
  }

  doNotUseCashVoucherUtil() {
    this.payOnline();
  }

  payOnline() {
    this.showSpinner = true;
    console.log(this.paymentStatus);
    this.httpService.postRequest(GET_PAYMENT_DETAILS+'?sum='+this.remainingBetAmount, {}).subscribe(
        data => {
          setTimeout(() => {
            this.showSpinner = false;
          }, 2000);
          window.location.href = data['redirect_url'];
          // this.router.navigate(["print/" + this.paymentId + "/" + this.payerId + "/" + this.actualAmount]);

          // this.router.navigateByUrl(data['redirect_url']);          
        },
        error => { 
          setTimeout(() => {
            this.showSpinner = false;
          }, 2000);
          console.log("Error while getting payment details", error);
        });
  }
  onprint() {
    const pdf1 = {name: 'nasie'};
    //let blob = new ([pdf], {type: 'application/pdf'});
    //const blobUrl = URL.createObjectURL(blob);
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    //console.log(this.route.snapshot.params['paymentId'] +"kkkk");
    iframe.src = "print/" + this.paymentId + "/" + this.payerId + "/" + this.actualAmount;
     console.log(iframe.src);
    document.body.appendChild(iframe);
    iframe.contentWindow.print();
     console.log(iframe.contentWindow);

    // let printContents, popupWin;
    // printContents = document.getElementById('print-section').innerHTML;
    // popupWin = window.open('', '_blank', 'top=0,left=0,height=50%,width=auto');
    // popupWin.document.open();
    // popupWin.document.write(`
    //   <html>
    //     <head>
    //       <title>Print tab</title>
    //       <style>
    //       //........Customized style.......
    //       </style>
    //     </head>
    // <body onload="window.print();window.close()">${printContents}</body>
    //   </html>`
    // );
    // popupWin.document.close();
    
    // this.authService.logoutFinal.next();
    // console.log("jjjj");
    // console.log(localStorage);


    //this.router.navigate(["print/" + this.paymentId + "/" + this.payerId + "/" + this.actualAmount]);

   }

}

