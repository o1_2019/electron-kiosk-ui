import { Component, OnInit } from '@angular/core';
import { raceInfo } from '../models/race-info.model';
import { BettingService } from '../_services/betting.service';
import { RaceInfoService } from './../_services/race-service/race-info.service';
import { HttpService } from '../_services/http.service';
import { GET_RACE_DETAILS } from '../_constants/app.constants';
import { SocketsService } from '../_services/sockets.service';

@Component({
  selector: 'app-race-list',
  templateUrl: './race-list.component.html',
  styleUrls: ['./race-list.component.scss']
})
export class RaceListComponent implements OnInit {
  raceList: Array<{}>;
  showSpinner: boolean = true;

  constructor(private bettingService: BettingService,
              private httpService: HttpService,
              private socketService: SocketsService) { }

  ngOnInit() {

    this.socketService.getRaceData().then(resultData => {
      this.showSpinner = false;

      // this.raceList = JSON.parse("result", resultData["data"]);
      let tempVal = JSON.parse(resultData['data']);
      // console.log("inside component", JSON.parse(tempVal['response']));
      this.raceList = JSON.parse(tempVal['response']);
    }, error => {
      console.error(error);
    });

    // this.httpService.getRequest(GET_RACE_DETAILS).subscribe(data => {
      //  this.raceList = data["response"];
      //  console.log("http request - ", this.raceList);
// 
    // });
    // this.httpService.getRequest(GET_RACE_DETAILS).subscribe(data => {
      
      // setTimeout(() => {
        // this.showSpinner = false;
      //   this.raceList = data["response"];
      //   console.log(this.raceList);
        
      // }, 2000);
       

    // });
    // console.log(this.raceList);
  }

}
