import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CashOutComponent } from './cash-out/cash-out.component';
import { TopRacesComponent } from './top-races/top-races.component';
import { MyBetsComponent } from './my-bets/my-bets.component';
import { RaceInfoComponent } from './race-info/race-info.component';
import { LoginComponent } from './login/login.component';
import { ScannerComponent } from './scanner/scanner.component';
import { RaceListComponent } from './race-list/race-list.component';
import { PlaceOrderComponent } from './place-order/place-order.component';
import { PaymentStatusComponent } from './payment-fail/payment-fail.component';
import { PrintComponent } from './place-order/print/print.component';
// import { CookieModule } from 'ngx-cookie';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'login/:auth', component: LoginComponent },
  { path: 'scanner', component: ScannerComponent },
  { path: 'cancel', component: PaymentStatusComponent },
  { path: 'order', component: PlaceOrderComponent },
  { path: 'print/:id/:pay/:amnt', component: PrintComponent },
  { path: 'home', component: NavigationComponent,
    children: [
      { path: '', redirectTo: 'raceslist', pathMatch: 'full' },
      // { path: 'races', component: RaceInfoComponent },
      { path: 'raceslist', component: RaceListComponent },
      { path: 'cashout', component: CashOutComponent },
      { path: 'topraces', component: TopRacesComponent },
      { path: 'mybets', component: MyBetsComponent }
    ]
  },
  // { path: 'races', redirectTo: 'home/races', pathMatch: 'full' },
  // { path: 'cashout', redirectTo: 'home/cashout', pathMatch: 'full' },
  // { path: 'topraces', redirectTo: 'home/topraces', pathMatch: 'full' },
  // { path: 'mybets', redirectTo: 'home/mybets', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
