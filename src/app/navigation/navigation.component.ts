import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { BluetoothCore } from '@manekinekko/angular-web-bluetooth';
import { BettingService } from '../_services/betting.service';
import { FormControl, Validators } from '@angular/forms';
import { HttpService } from '../_services/http.service';
import { CHECKOUT_URL } from '../_constants/app.constants';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  bets: Array<{}>;
  menuId: number;
  values = 0;
  betAmount: number = 0;
  singeBetAmount: number = 0;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute,
    private bettingService: BettingService,
    private httpService: HttpService) {}

  ngOnInit() {
    this.bets = this.bettingService.getBetsAdded();
    this.bettingService.removeSingleBet.subscribe((bet: any) => {
      this.bettingService.removeBet(bet);
      if (bet.value.betAmount > 0) {
        this.betAmount -= bet.value.betAmount;
      }
    });
    this.bettingService.perBetAmount.subscribe((diff: any) => {
      this.betAmount += diff;
      console.log(this.betAmount);
    });
  }

  doLogOut() {
    this.router.navigate(['/'],  {relativeTo: this.route});
  }

  onKey(betObj, event: any) { // without type info
    console.log(betObj);
    betObj.value.betAmount = +event.target.value;
    this.singeBetAmount = betObj.value.betAmount;
  }

  saveValue(betObj) {
    console.log(betObj);
  }

  removeBet(betObj) {
    this.bettingService.removeBet(betObj);
    // console.log(this.bettingService.getBetsAdded());
  }

  placeBet() {
    console.log("place bet - ", this.bets);
    // let reqBody = {
    //   'betsPlaced': this.bets
    // };
    let totalBetAmount = 0;
    this.bets.forEach(bet => {
      totalBetAmount += bet["value"].betAmount;
    });
    this.betAmount = totalBetAmount;
    this.router.navigate(['/order'], { queryParams: { amount: totalBetAmount } });

    // this.httpService.postRequest(CHECKOUT_URL, reqBody).subscribe(
    //   data => {
    //     console.log("Checkout successful ", data);
    //   },
    //   error => {
    //     console.log("Login error", error);
    //   });
  }
  
  showNoOfBets() {
    if (this.bets.length > 0) {
      return String(this.bets.length);
    } else {
      return "";
    }
  }
}
