import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../_services/http.service';
import { AuthenticationService } from '../_services/authentication.service';
import { FileService } from '../_services/file.service';
import { TOKEN_NAME } from '../_constants/app.constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  accountDetails = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private fileService: FileService
  ) { }

  ngOnInit() {

    // //this.accountDetails =  localStorage.getItem('Username');
    // const index = this.accountDetails.indexOf('@');
    // this.accountDetails = this.accountDetails.substring(0, index);
    this.authenticationService.loginEmail.subscribe((data: string) => {
        // console.log('Mya adta' + data);
        const index = data.indexOf('@');
        this.accountDetails = data.substring(0, index);
    });

    // this.fileService.getFiles().then(console.log);
  }

  doLogOut() {
    localStorage.removeItem(TOKEN_NAME);
    localStorage.removeItem("decodedToken");
    this.router.navigate(['/'],  {relativeTo: this.route});
    this.authenticationService.logoutFinal.subscribe(() => {
      //alert("ssss");
      localStorage.removeItem(TOKEN_NAME);
      localStorage.removeItem("decodedToken");
      this.router.navigate(['/'],  {relativeTo: this.route});
    });
  }

  isAuthenticated() {
    return this.authenticationService.isAuthenticated();
  }
}
