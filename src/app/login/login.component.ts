import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../_services/authentication.service';
import { relative } from 'path';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  notAllowed = false;

  constructor(private router: Router,
              private route: ActivatedRoute, 
              private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.loginAllowed.subscribe(() => {
      //alert("called");
      console.log(this.notAllowed);
      //this.router.navigate(['/login'], { relativeTo: this.route});
      //this.notAllowed = true;
      setTimeout(() => {
       // this.router.navigate(['/login', true] , { relativeTo: this.route});
        this.notAllowed = true;
        //this.router.navigate(['/login/klklk'], {relativeTo: this.route});
        //alert("cha");
      }, 2000);
    });

  }

  doLogIn() {
    this.router.navigate(['/scanner'], {relativeTo: this.route});
  }
}
