import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetSlipCardComponent } from './bet-slip-card.component';

describe('BetSlipCardComponent', () => {
  let component: BetSlipCardComponent;
  let fixture: ComponentFixture<BetSlipCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetSlipCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetSlipCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
