export const CHECKOUT_URL = "https://reqres.in/api/users";

export const LOGIN_URL: string = 'https://reqres.in/api/users';
export const LOGIN_AUTH: string = 'http://192.168.43.40:8092/api/login/kiosk/qrcode/verification';

export const SCAN_VOUCHER_URL: string = 'http://192.168.43.16:8096/cashvoucher/validation';

export const GET_PAYMENT_DETAILS: string = 'http://192.168.43.40:8095/api/paypal/make/payment';

export const GET_RACE_DETAILS: string = "http://192.168.43.40:8095/api/details/race/all";  //"https://192.168.43.40:7443/details/race/all";
export const GET_HORSE_DETAILS: string = "http://192.168.43.40:8095/api/details/horse";

export const ORDER_UPDATE_URL: string = "http://192.168.43.40:8095/api/purchase/user";

export const TOKEN_NAME: string = 'accessToken';

export const RACE_SOCKET_URL: string = 'ws://192.168.43.40:8095/details/race/all/socket';

export const HORSE_SOCKET_URL: string = 'ws://192.168.43.40:8095/socket/details/horse';
