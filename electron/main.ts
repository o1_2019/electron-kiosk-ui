import { app, BrowserWindow, ipcMain } from "electron";
import * as path from "path";
import * as url from "url";
import * as fs from "fs";
// import * as electronReload from "electron-reload";
const electron = require('electron')
// const bluetooth = require('node-bluetooth');

// const device = new bluetooth.DeviceINQ();


// Enable live reload for all the files inside your project directory
// require('electron-reload')(__dirname);
electron.app.commandLine.appendSwitch('ignore-certificate-errors', 'true');

// import * as noble from "noble";

// electronReload(__dirname);

let win: BrowserWindow;

app.on("ready", createWindow);

app.on("activate", () => {
  if (win === null) {
    createWindow();
  }
});

function createWindow() {
    win = new BrowserWindow({ width: 800, height: 600 });

    
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, `/../../dist/electron-kiosk-app/index.html`),
        protocol: "file:",
        slashes: true
      })
    );
  
    win.webContents.openDevTools();
  

    win.on("closed", () => {
      win = null;
    });
  }


  ipcMain.on("sendBluetoothMsg", (event, arg1) => {
    // win.webContents.print();
    console.log("received 2", arg1);
    
    let python = require('child_process').spawn('python', ['./bluetooth/bluetooth_app.py', arg1]);
    python.stdout.on('data',function(data){
        data = data.toString('utf8');
        console.log("data: ",data);
        win.webContents.send("bluetoothToken", data);
    });

    // device.listPairedDevices(console.log);

    // device
    //   .on('finished',  console.log.bind(console, 'finished'))
    //   .on('found', function found(address, name){
    //     console.log('Found: ' + address + ' with name ' + name);

    //     device.findSerialPortChannel(address, function(channel) {
    //         console.log('Found RFCOMM channel for serial port on ', name, ': ', channel);

    //         // make bluetooth connect to remote device
    //         bluetooth.connect(address, 2, function(err, connection) {

    //             if (err) return console.error(err);
    //             connection.write(new Buffer('Hello!', 'utf-8'), () => {
    //               console.log("wrote");
    //             });
    //         });

    //     });

    //   }).scan();

  });