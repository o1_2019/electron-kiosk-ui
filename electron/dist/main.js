"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var path = require("path");
var url = require("url");
// import * as electronReload from "electron-reload";
var electron = require('electron');
// const bluetooth = require('node-bluetooth');
// const device = new bluetooth.DeviceINQ();
// Enable live reload for all the files inside your project directory
// require('electron-reload')(__dirname);
electron.app.commandLine.appendSwitch('ignore-certificate-errors', 'true');
// import * as noble from "noble";
// electronReload(__dirname);
var win;
electron_1.app.on("ready", createWindow);
electron_1.app.on("activate", function () {
    if (win === null) {
        createWindow();
    }
});
function createWindow() {
    win = new electron_1.BrowserWindow({ width: 800, height: 600 });
    win.loadURL(url.format({
        pathname: path.join(__dirname, "/../../dist/electron-kiosk-app/index.html"),
        protocol: "file:",
        slashes: true
    }));
    win.webContents.openDevTools();
    win.on("closed", function () {
        win = null;
    });
}
electron_1.ipcMain.on("sendBluetoothMsg", function (event, arg1) {
    // win.webContents.print();
    console.log("received 2", arg1);
    var python = require('child_process').spawn('python', ['./bluetooth/bluetooth_app.py', arg1]);
    python.stdout.on('data', function (data) {
        data = data.toString('utf8');
        console.log("data: ", data);
        win.webContents.send("bluetoothToken", data);
    });
    // device.listPairedDevices(console.log);
    // device
    //   .on('finished',  console.log.bind(console, 'finished'))
    //   .on('found', function found(address, name){
    //     console.log('Found: ' + address + ' with name ' + name);
    //     device.findSerialPortChannel(address, function(channel) {
    //         console.log('Found RFCOMM channel for serial port on ', name, ': ', channel);
    //         // make bluetooth connect to remote device
    //         bluetooth.connect(address, 2, function(err, connection) {
    //             if (err) return console.error(err);
    //             connection.write(new Buffer('Hello!', 'utf-8'), () => {
    //               console.log("wrote");
    //             });
    //         });
    //     });
    //   }).scan();
});
//# sourceMappingURL=main.js.map